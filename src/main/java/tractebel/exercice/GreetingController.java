package tractebel.exercice;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class GreetingController {

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
    
    /*
    @RequestMapping(value = "/pizza/configuration/{id}", method = RequestMethod.GET)
    public String pizzaConfig(@PathVariable("id") String id) {
        return String.format("query sent to the database with id:{}",id); //query result with device Id;
    }
    */


}
