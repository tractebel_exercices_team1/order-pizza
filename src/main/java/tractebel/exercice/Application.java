package tractebel.exercice;

import java.sql.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	
    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
        getRemoteConnection();        
        
    }
    
    private static Connection getRemoteConnection() {
        if (System.getenv("RDS_HOSTNAME") != null) {
          try {
          Class.forName("org.postgresql.Driver");
          String dbName = System.getenv("RDS_DB_NAME");
          String userName = System.getenv("RDS_USERNAME");
          String password = System.getenv("RDS_PASSWORD");
          String hostname = System.getenv("RDS_HOSTNAME");
          String port = System.getenv("RDS_PORT");
          String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password;
          System.out.println("Getting remote connection with connection string from environment variables.");
          Connection con = DriverManager.getConnection(jdbcUrl);
          System.out.println("Remote connection successful.");
          return con;
        }
        catch (ClassNotFoundException e) {System.out.println(e.toString());}
        catch (SQLException e) { System.out.println(e.toString());}
        }
        return null;
      }
    

}
